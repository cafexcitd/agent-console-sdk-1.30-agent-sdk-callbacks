<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://192.168.0.99:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://192.168.0.99:8443/assistserver/sdk/web/shared/css/shared-window.css">

    <style type="text/css">

        #local {
            width: 160px;
            height: 120px;
        }

        #remote {
            width: 320px;
            height: 240px;
        }

        #local, #remote {
            border: 1px solid grey;
        }

        #quality {
            height: 40px;
            width: 100px;
        }

        #quality.call-quality-good {
            background-color: green;
        }

        #quality.call-quality-moderate {
            background-color: yellow;
        }

        #quality.call-quality-poor {
            background-color: red;
        }

    </style>
</head>

<body>

<h3>Video</h3>

<!-- remote video view -->
<div id="remote"></div>

<!-- local video preview -->
<div id="local"></div>

<!-- button through which agent clicks to end call -->
<button id="end">End</button>

<!-- indicates the quality of the call -->
<div id="quality">Call Quality</div>

<!-- libraries needed for Assist SDK -->
<script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>
<script src="http://192.168.0.99:8080/gateway/adapter.js"></script>
<script src="http://192.168.0.99:8080/gateway/csdk-sdk.js"></script>
<script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/assist-aed.js"></script>
<script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/shared-windows.js"></script>
<script src="http://192.168.0.99:8080/assistserver/sdk/web/agent/js/assist-console.js"></script>
<script src="http://192.168.0.99:8080/assistserver/sdk/web/agent/js/assist-console-callmanager.js"></script>



<!-- load jQuery - helpful for DOM manipulation -->
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- control -->
<script>

    <?php
    // since we know that the agent will require to be provisioned, we
    // can eagerly provision a session token on the page load rather
    // than do so lazily later

    // include the Provisioner class we created to do the provisioning
    require_once('Provisioner.php');
    $provisioner = new Provisioner;
    $token = $provisioner->provisionAgent(
        'agent1',
        '192.168.0.99',
        '.*');

    // decode the sessionID from the provisioning token
    $sessionId = json_decode($token)->sessionid;
    ?>

    var sessionId = '<?php echo $sessionId; ?>';
    console.log("SessionID: " + sessionId);

    var AgentModule = function (sessionId) {
        // Declare variables needed during the call
        var
        // native DOM elements
            remote,
            local,
            quality,
        // jQuery elements for easier event binding
            $end;


        // organise functions to do important tasks...
        cacheDom(); // cache the DOM elements we will need later during the call
        setClickHandlers();  // define what happens when our app's buttons are clicked
        bindAgentSdkCallbacks();  // set all of the agentSdk's call-back listeners
        linkUi(); //
        // ...then, with everything prepared and ready ...
        init(sessionId);  // init the library and register the agent to receive calls


        // caches the DOM elements we will need later during the call
        function cacheDom() {
            // extract native DOM elements - AgentSDK works with DOM elements directly
            remote = $('#remote')[0];
            local = $('#local')[0];
            quality = $('#quality')[0];
            // cache as jQuery objects for easier UI event handling
            $end = $('#end');
        }

        // add click handlers that determine what happens when agent clicks buttons, etc
        function setClickHandlers() {
            // handle end call button being clicked
            $end.click(function(event) {
                // clear the remove video view - optional
                $(remote).find('video').attr('src', '');
                // end the call
                CallManager.endCall();
            });
        }

        // set all of the agentSdk's call-back listeners
        function bindAgentSdkCallbacks(){
            // Connection events
            AssistAgentSDK.setConnectionEstablishedCallback(function () {
                console.log('----------- setConnectionEstablishedCallback');
            });

            AssistAgentSDK.setConnectionLostCallback(function () {
                console.log('----------- setConnectionLostCallback');
            });

            AssistAgentSDK.setConnectionReestablishedCallback(function () {
                console.log('----------- setConnectionReestablishedCallback');
            });

            AssistAgentSDK.setConnectionRetryCallback(function (retryCount, retryTimeInMilliSeconds) {
                console.log('----------- setConnectionRetryCallback');
            });

            // Call event
            AssistAgentSDK.setConsumerEndedSupportCallback(function () {
                console.log('----------- setConsumerEndedSupportCallback');
                // clear the remove video view - optional
                $(remote).find('video').attr('src', '');
            });

            // Assist feature events
            AssistAgentSDK.setFormCallBack(function (form) {
                console.log('setFormCallBack');
            });

            AssistAgentSDK.setRemoteViewCallBack(function (x, y) {
                console.log('setRemoteViewCallBack');
                console.log('x: ' + x + ', y: ' + y);
            });

            AssistAgentSDK.setScreenShareActiveCallback(function(active) {
                console.log('setScreenShareActiveCallback active: ' + active);
            });

            AssistAgentSDK.setSnapshotCallBack(function (snapshot) {
                console.log('setSnapshotCallBack');
            });
        }

        // links UI elements to their Agent SDK outlets
        function linkUi() {
            // video calling elements
            CallManager.setRemoteVideoElement(remote);
            CallManager.setLocalVideoElement(local);
            CallManager.setCallQualityIndicator(quality);
        }

        // initialises using the Agent SDK CallManager
        function init(sessionId) {

            var gatewayUrl = 'http://192.168.0.99:8080';
            CallManager.init({
                sessionToken: sessionId,
                autoanswer: false,
                username: 'agent1',
                password: 'none',
                agentName: 'Agent',
                agentPictureUrl: gatewayUrl + '/assistserver/img/avatar.png',
                url: gatewayUrl
            });
        }

    }(sessionId);

</script>
</body>
</html>
